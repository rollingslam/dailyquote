package hk.edu.ouhk.android.s326finalproj;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.awt.font.TextAttribute;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();
    public String URL = "https://quotes.rest/";
    public String ACTION;
    public String URLrequest;

    RecyclerView recyclerView;
    CardView TdyQuote;
    BottomNavigationView bottomNavigationView;


    String Categories[], CatDescription[];
    int images[] = {R.drawable.ic_lightbulb_outline,R.drawable.ic_library_books,R.drawable.ic_directions_run,R.drawable.ic_people,
            R.drawable.ic_sentiment_very_satisfied,R.drawable.ic_healing,R.drawable.ic_crop_original,R.drawable.ic_school};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        showRecyclerList();
        actionBottomNavBar();
    }

    private void showRecyclerList(){
        TdyQuote = findViewById(R.id.todayQuote);
        TdyQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, hk.edu.ouhk.android.s326finalproj.ShowQuote.class);
                intent.putExtra("ACTION", ACTION);
                startActivity(intent);
            }
        });

        recyclerView = findViewById(R.id.recyclerviewCat);

        Categories = getResources().getStringArray(R.array.categories);
        CatDescription = getResources().getStringArray(R.array.cat_description);

        CatAdapter adapter = new CatAdapter(this, Categories, CatDescription, images);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
    }

    private void actionBottomNavBar(){
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_home:
                        finish();
                        startActivity(getIntent());
                        //Toast.makeText(MainActivity.this, "Nav Home", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_settings:
                        Intent intent = new Intent(MainActivity.this, hk.edu.ouhk.android.s326finalproj.Settings.class);
                        startActivity(intent);
                        //Toast.makeText(MainActivity.this, "Nav Settings", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_star:
                        Toast.makeText(MainActivity.this, "Nav Star", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
    }

}
