package hk.lucas.lam.android.dailyquote;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

public class CatAdapter extends RecyclerView.Adapter<CatAdapter.ViewHolder> {

    private String TAG = CatAdapter.class.getSimpleName();

    String categories[] = {"inspire", "management", "sports", "life", "funny", "love", "art", "students"};
    String catTitle[], catDesc[];
    int images[];
    Context context;

    public CatAdapter(Context ct, String titles[], String desc[], int img[]){
        context = ct;
        catTitle = titles;
        catDesc = desc;
        images = img;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.list_item,parent,false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.Categories.setText(catTitle[position]);
        holder.CatDescription.setText(catDesc[position]);
        holder.CatImage.setImageResource(images[position]);

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ShowQuote.class);
                Log.i(TAG, "PROCEDURE: categories " + categories[position]);
                intent.putExtra("ACTION", categories[position]);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        //Log.i(TAG, "PROCEDURE: title length " + catTitle.length);
        return catTitle.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView Categories, CatDescription;
        ImageView CatImage;
        LinearLayout mainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            Categories = itemView.findViewById(R.id.category);
            CatDescription = itemView.findViewById(R.id.catDescription);
            CatImage = itemView.findViewById(R.id.catImage);
            mainLayout = itemView.findViewById(R.id.mainLayout);
        }
    }
}
