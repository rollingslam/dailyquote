package hk.lucas.lam.android.dailyquote;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ShowQuote extends AppCompatActivity {

    private String TAG = ShowQuote.class.getSimpleName();
    public String URL = "https://quotes.rest/qod.json";
    public String ACTION;
    public String URLrequest;

    private String QuoteClip;
    private String PermaLink;
    private String imgURL;

    TextView QuoteContent, QuoteAuthor;
    ImageView QuoteImage;
    Button KnowMore;
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.showquote);

        Intent intent = getIntent();
        ACTION = intent.getStringExtra("ACTION");
        Log.i(TAG, "PROCEDURE: " + ACTION);

        actionBottomNavBar();
        extractContent();
        actionTrigger();

    }

    private void actionBottomNavBar(){
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_home:
                        Intent intent = new Intent(ShowQuote.this, MainActivity.class);
                        startActivity(intent);
                        //Toast.makeText(MainActivity.this, "Nav Home", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_settings:
                        Intent intent2 = new Intent(ShowQuote.this, hk.edu.ouhk.android.s326finalproj.Settings.class);
                        startActivity(intent2);
                        //Toast.makeText(MainActivity.this, "Nav Settings", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_star:
                        Toast.makeText(ShowQuote.this, "Nav Star", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
    }

    private void actionTrigger(){
        QuoteContent = (TextView) findViewById(R.id.quoteContent);
        QuoteContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Copy Text", QuoteClip);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(ShowQuote.this, "Copied to clipbaord", Toast.LENGTH_SHORT).show();
            }
        });

        QuoteAuthor = (TextView) findViewById(R.id.quoteAuthor);
        QuoteAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Copy Text", QuoteClip);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(ShowQuote.this, "Copied to clipbaord", Toast.LENGTH_SHORT).show();
            }
        });

        KnowMore = (Button) findViewById(R.id.knowMore);
        KnowMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(PermaLink));
                startActivity(browserIntent);
            }
        });

    }

    private void extractContent(){

        final ArrayList<String> contentTAGS = new ArrayList<String>();

        if(ACTION != null) {
            URLrequest = URL + "?category=" + ACTION;
        }
        else{
            URLrequest = URL;
        }

        Log.i(TAG, "PROCEDURE: URL " + URLrequest);

        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URLrequest, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "PROCEDURE: onResponse");
                Log.i(TAG, "PROCEDURE: " + response);

                try {
                    JSONObject contents = response.getJSONObject("contents");
                    JSONArray quotes = contents.getJSONArray("quotes");

                    Log.i(TAG, "PROCEDURE: length: " + quotes.length());
                    for(int i = 0; i < quotes.length(); i++){

                        JSONObject q = quotes.getJSONObject(i);

                        String quote = q.getString("quote");
                        String length = q.getString("length");
                        String author = q.getString("author");

                        QuoteClip = quote + "\n " + author;

                        Log.i(TAG, "PROCEDURE: \n quote: " + quote + "\n length: " + length + "\n author: " + author);

                        QuoteContent.setText(quote);
                        QuoteAuthor.setText(author);


                        /*
                        JSONArray tags = q.getJSONArray("tags");

                        Log.i(TAG, "PROCEDURE: " + tags);
                        Log.i(TAG, "PROCEDURE: " + tags.length());
                        for(int j = 0; j < tags.length(); j++){
                            String t = tags.getString(j);
                            Log.i(TAG, "PROCEDURE: " + t);
                            contentTAGS.add(t);
                        }

                         */

                        String category = q.getString("category");
                        String language = q.getString("language");
                        String date = q.getString("date");
                        String permalink = q.getString("permalink");
                        String id = q.getString("id");
                        String background = q.getString("background");
                        String title = q.getString("title");

                        PermaLink = permalink;
                        imgURL = background;

                        Log.i(TAG, "PROCEDURE: \n category: " + category + "\n language: " + language + "\n date: " + date
                                + "\n permalink: " + permalink + "\n id: " + id + "\n background: " + background + "\n title: " + title);

                        ImageView imageView = findViewById(R.id.quoteImage);
                        Log.i(TAG, "PROCEDURE: imgURL " + imgURL);
                        Picasso.with(ShowQuote.this).load(imgURL).into(imageView);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "PROCEDURE: onErrorResponse: " + error.getMessage());
            }
        });

        queue.add(jsonObjectRequest);

    }

}
