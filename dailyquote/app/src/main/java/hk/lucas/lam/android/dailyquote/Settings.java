package hk.edu.ouhk.android.s326finalproj;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Calendar;
import java.util.Set;

public class Settings extends AppCompatActivity implements View.OnClickListener {

    private String TAG = Settings.class.getSimpleName();

    private int notificationId = 1;
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        findViewById(R.id.setTime).setOnClickListener(this);
        findViewById(R.id.resetTime).setOnClickListener(this);

        actionBottomNavBar();
    }

    @Override
    public void onClick(View v) {
        TimePicker timePicker = findViewById(R.id.timePicker);

        Intent intent = new Intent(Settings.this, AlarmReceiver.class);
        intent.putExtra("notificationId", notificationId);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(Settings.this, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);

        switch(v.getId()){
            case R.id.setTime:
                int hour = timePicker.getCurrentHour();
                int minute = timePicker.getCurrentMinute();

                Calendar startTime = Calendar.getInstance();
                startTime.set(Calendar.HOUR_OF_DAY, hour);
                startTime.set(Calendar.MINUTE, minute);
                startTime.set(Calendar.SECOND, 0);
                long alarmStartTime = startTime.getTimeInMillis();

                alarm.set(AlarmManager.RTC_WAKEUP, alarmStartTime, alarmIntent);
                Toast.makeText(this, "Done", Toast.LENGTH_SHORT).show();
                break;

            case R.id.resetTime:
                alarm.cancel(alarmIntent);
                Toast.makeText(this, "Canceled", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void actionBottomNavBar(){
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_home:
                        Intent intent = new Intent(Settings.this, MainActivity.class);
                        startActivity(intent);
                        //Toast.makeText(Settings.this, "Nav Home", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_settings:
                        finish();
                        startActivity(getIntent());
                        //Toast.makeText(MainActivity.this, "Nav Settings", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_star:
                        Toast.makeText(Settings.this, "Nav Star", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
    }
}
