package hk.lucas.lam.android.dailyquote;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AlarmReceiver extends BroadcastReceiver {
    private String TAG = AlarmReceiver.class.getSimpleName();
    public String URL = "https://quotes.rest/qod.json";
    public String ACTION;
    public String URLrequest;
    public String QuoteMessage;

    private RequestQueue q;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onReceive(Context context, Intent intent) {
        int notificationId = intent.getIntExtra("notificationId", 0);
        String message = "A successful man is one who can lay a firm foundation with the bricks that others throw at him. By: Sidney Greenberg";

        Intent mainIntent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, mainIntent, 0);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


        //extractContent(context);
        if(QuoteMessage != null){
            message = QuoteMessage;
        }
        Log.i(TAG, "PROCEDURE: postExtract " + message);

        Notification.Builder builder = new Notification.Builder(context);
        builder.setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle("Your Daily Quote")
                .setContentText(message)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentIntent(contentIntent);

        notificationManager.notify(notificationId, builder.build());
    }


    private void extractContent(Context context){

        final ArrayList<String> contentTAGS = new ArrayList<String>();

        URLrequest = URL;

        Log.i(TAG, "PROCEDURE: URL " + URLrequest);

        q = Volley.newRequestQueue(context);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URLrequest, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "PROCEDURE: onResponse");
                Log.i(TAG, "PROCEDURE: " + response);

                try {
                    JSONObject contents = response.getJSONObject("contents");
                    JSONArray quotes = contents.getJSONArray("quotes");

                    Log.i(TAG, "PROCEDURE: length: " + quotes.length());
                    for(int i = 0; i < quotes.length(); i++){

                        JSONObject q = quotes.getJSONObject(i);

                        String quote = q.getString("quote");
                        String length = q.getString("length");
                        String author = q.getString("author");

                        QuoteMessage = quote + " By: " + author;
                        Log.i(TAG, "PROCEDURE: QuoteMessage " + QuoteMessage);

                        Log.i(TAG, "PROCEDURE: \n quote: " + quote + "\n length: " + length + "\n author: " + author);

                        String category = q.getString("category");
                        String language = q.getString("language");
                        String date = q.getString("date");
                        String permalink = q.getString("permalink");
                        String id = q.getString("id");
                        String background = q.getString("background");
                        String title = q.getString("title");

                        Log.i(TAG, "PROCEDURE: \n category: " + category + "\n language: " + language + "\n date: " + date
                                + "\n permalink: " + permalink + "\n id: " + id + "\n background: " + background + "\n title: " + title);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "PROCEDURE: onErrorResponse: " + error.getMessage());
            }
        });

        q.add(jsonObjectRequest);

    }

}
